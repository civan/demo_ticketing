from django.db import models
from django.contrib.auth.models import User

# Create your models here.

'''
Definición de un modelo básico de Tickets
'''
class Ticket(models.Model):
    ESTADO = (
        ('1', 'ABIERTO'),
        ('2', 'ASIGNADO'),
        ('3', 'CERRADO'),
    )

    titulo = models.CharField(max_length=250)
    descripcion = models.TextField()
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    usuario_creado = models.ForeignKey(User, related_name='usuario_creado')
    usuario_asignado = models.ForeignKey(User, related_name='usuario_asignado', blank=True, null=True)
    estado = models.CharField(
        max_length=1,
        choices=ESTADO,
        default='1',
    )

    def __str__(self):
        return self.titulo
