from django import forms
from .models import Ticket

'''
Definición de nuestro formulario de Ticket genérico para todos los usuarios.
Luego en la gestión del panel del admin para el modelo Ticket, haremos un
control de usuarios y una modificación al vuelo de los campos que se exclyen
según los permisos
'''
class TicketForm(forms.ModelForm):

    class Meta:
        fields = ['titulo', 'descripcion', 'usuario_creado', 'usuario_asignado', 'estado', ]
        model = Ticket
